Su Windows:

* Installa il software ASEBA per Thymio (https://aseba.wdfiles.com/local--files/en:wininstall/aseba-1.6.1.exe)
* Aprire bin/asebascratch.exe
* Aprire SNAP su un browser e caricare il progetto thymio.xml

thymio.xml è il file principale, ovvero il progetto SNAP da cui partire, contenente i blocchetti per comandare il Thymio

thymio_snap_09092017.xml è un progetto vecchio sviluppato da MOBSYA

bridge.aesl è uno sketch per thymio che va caricato sul thymio per far funzionare l'estensione di snap con il web bridge nuovo di aseba studio suite
